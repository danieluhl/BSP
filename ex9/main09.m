% -------------------------------------------------------------------------
% Biosignal Processing - Excercise 09
% Group members:
% + Patrick Schuster - 51811485
% + Daniel Uhl - 11730101
% -------------------------------------------------------------------------

% clean workspace
clear; 
clc;
close all;
%% Task 1
L = 10; %s
fs = 100; %Hz
f0 = 3.4; % Hz

t = [0:1/fs:L]
N = length(t);

f_norm = (-N/2:1:(N-1)/2).'/N;

test_signal_without_trend = sin(2*pi*f0*t)

offset = 0.2
linear_trend = 0.1

test_signal_with_trend = test_signal_without_trend + offset + linear_trend*t



%% a) using detrend
detrended_signal_matlab = detrend(test_signal_with_trend);

%% b) filter in time domain
[b_detrend, a_detrend] = butter(2, f0/fs, 'high');
detrended_signal_filter = filter(b_detrend, a_detrend, test_signal_with_trend);


figure(100);
plot(t, test_signal_without_trend);
title("Test signal original");
xlabel("t in s")
ylabel("Magnitude")

figure(101);
plot(t, test_signal_with_trend);
title("Test signal with trend");
xlabel("t in s")
ylabel("Magnitude")

figure(102);
plot(t, detrended_signal_matlab);
title("Detrended with matlab");
xlabel("t in s")
ylabel("Magnitude")
figure(103);
plot(t, detrended_signal_filter);
title("Detrended with Butterworth filter");
xlabel("t in s")
ylabel("Magnitude")
figure(104);
plot(f_norm*fs, fftshift(abs(fft(test_signal_with_trend))));
title("Spectrum of biased test signal");
xlabel("f in Hz")
ylabel("Magnitude")
figure(105);
plot(f_norm*fs, fftshift(abs(fft(detrended_signal_filter))));
title("Spectrum of detrended signal");
xlabel("f in Hz")
ylabel("Magnitude")

%%%%%%%%%%%
% Task 2
%%%%%%%%%%%

fs = 50; % sampling frequency in Hz
f0 = 1;  % fundamental frequency of the test signal in Hz
A = 1;   % amplitude in V
L = 1e3; % signal duration in seconds

t = (0:1/fs:L-1/fs).';              % observation time vector
y = A*sin(2*pi*f0.*t);              % single sine wave
yNoisy = awgn(y, 3, 'measured');    % add white Gaussian noise
noiseFloor = yNoisy - y;            % get noise only, by subtraction known signal
mean(noiseFloor)
sqrt(var(noiseFloor)) % standard deviation

% plot two periods of the sine waves
vView = (1:101).';
figure;
plot(t(vView), y(vView)), grid on, hold on;
plot(t(vView), yNoisy(vView)), hold off;
axis tight, legend('clean signal', 'noisy signal');
xlabel('Time in seconds'), ylabel('Amplitude in V');

Lwin = fs/f0; % samples per wave -> observation length for one period
vWin = (1:Lwin).';
Psig = bandpower(y(vWin));              % ideal signal power
Pnoise = bandpower(noiseFloor(vWin));   % noise power added to the signal

SNRref = Psig/Pnoise;

%% Averaging

vN = [25 50 250 1000].';    % #averages of interest 
yAvg = zeros(L, Lwin);      % averaged signals
yAligned = zeros(L, Lwin);  % align single periods for row-wise sum
slidingWin = vWin;
SNRmeas = zeros(L, 1);
stdDnoise = zeros(L, 1); 
for j = 1:L
    yAligned(j,:) = yNoisy(slidingWin);
    slidingWin = (slidingWin(end)+1:slidingWin(end)+Lwin).'; % shift window by one win-length
    yAvg(j,:) = sum(yAligned)./j; 
    
    % calculate individual powers and std. Dev. Noise
    Psig = bandpower(yAvg(j,:));    
    Pnoise = bandpower(yAvg(j,:) - y(vWin).');
    SNRmeas(j) = Psig/Pnoise;
    stdDnoise(j) = sqrt(var(yAvg(j,:) - y(vWin).'));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the averages
% plot one period of the sine wave
figure;
plot(t(vWin), y(vWin)), grid on, hold on, axis tight;
plot(t(vWin), yNoisy(vWin))
for k = 1:length(vN)
    plot(t(vWin), yAvg(vN(k),:))
end
legend('clean signal', 'noisy signal', '25. Avg', '50. Avg', ... 
            '250. Avg', '1000. Avg');
xlabel('Time in seconds'), ylabel('Amplitude in V');
title('Noise Reduction through Averaging');

figure;
plot(t(vWin), y(vWin)), grid on, hold on, axis tight;
c = ['r', 'g'];
for k = 3:length(vN)
    plot(t(vWin), yAvg(vN(k),:), 'Color', c(k-2))
end
legend('clean signal', '250. Avg', '1000. Avg');
xlabel('Time in seconds'), ylabel('Amplitude in V');
title('Noise Reduction through Averaging');

%%%%%%%%
% SNR
figure, plot(SNRmeas), grid on;
title("Signal-to-noise Ratio");
xlabel('#averages'), ylabel('P_{Signal}/P_{Noise}');

figure, plot(10.*log10(SNRmeas)), grid on;
title("Signal-to-noise Ratio - log scale");
xlabel('#averages'), ylabel('P_{Signal}/P_{Noise} in dB');
axis([-25 L -0.1 35]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Standard Deviation Noise
figure, plot(stdDnoise), grid on, hold on;
pStdD = plot(1:L, 1./sqrt(1:L), '.k', 'MarkerSize', 2);
pStdD.Color(4) = 0.5;
legend('\sigma_{Noise}', '1/sqrt(N)');
title("Standard Deviation Noise");
xlabel('#averages'), ylabel('Standard Deviation Noise in V');
axis([-25 L -0.1 1.1]);

set(figure(100), 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(figure(100), 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(figure(100), "ex9-1-test_signal_original.pdf")


set(figure(101), 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(figure(101), 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(figure(101), "ex9-1-test_signal.pdf")


set(figure(102), 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(figure(102), 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(figure(102), "ex9-1-detrended_matlab.pdf")

set(figure(103), 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(figure(103), 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(figure(103), "ex9-1-detrended_filter.pdf")

set(figure(104), 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(figure(104), 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(figure(104), "ex9-1-fft.pdf")

set(figure(105), 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(figure(105), 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(figure(105), "ex9-1-fft_detrended.pdf")