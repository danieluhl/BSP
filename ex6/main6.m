% -------------------------------------------------------------------------
% Biosignal Processing - Excercise 05
% Group members:
% + Patrick Schuster - 51811485
% + Daniel Uhl - 11730101
% -------------------------------------------------------------------------

% clean workspace
clear; 
clc;
close all;
%%

%% a) loads US population example
% vars: cdate, pop
load('census') 
% consider only population until 1900s
N = length(cdate(cdate <= 1900));
cdateObs = cdate(cdate <= 1900);
popObs = pop(1:N);

%% b) Apply MATLAB polyfit
% p1*x^n + p2*x^(n-1) + ... + pn*x + p_(n+1)
% n=2 --> p1*x^2 + p2*x + p3
[p, ErrorEst] = polyfit(cdateObs, popObs, 2);
pop_fit = polyval(p, cdateObs, ErrorEst);

%% c) Apply self-written function
[m, y] = ls_fit([cdateObs, popObs], 2);

%% d)
figure;
plot(cdateObs, popObs, '-o'), grid on, hold on
plot(cdateObs, pop_fit, '-s'), hold on
plot(cdateObs, y, '-x');
hold off, legend('Observation', 'polyFit (2nd Order)', 'LS (2nd Order)', 'location', 'NorthWest')
title(sprintf('US population from %d to %d',cdate(1), cdate(N)))
xlabel('Census Year'), ylabel('Population (millions)')
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex6-2-d.pdf")

%% e)


figure;
res_polyfit = popObs - pop_fit;
res_lsfit   = popObs - y;


hold on; grid on;
title("Residuals");
stem(cdateObs, res_polyfit);
stem(cdateObs, res_lsfit);
legend(sprintf("2nd order polyfit, L2-norm = %.3f", norm(res_polyfit, 2)), ...
        sprintf("2nd order lsfit, L2-norm = %.3f", norm(res_lsfit, 2)))
xlabel('Census Year'), ylabel('Population (millions)')
hold off;
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex6-2-e.pdf")

%% f) prediction
cdatePred = [1950, 1970, 1990].';

Gpred = [ones(length(cdatePred),1), cdatePred, cdatePred.^2];

popPred = [y; Gpred*m]
figure;
plot(cdate, pop, 'b-o'), hold on
plot([cdateObs], y, 'r-', 'LineWidth', 2)
plot([cdatePred], Gpred*m, 'o','MarkerFaceColor','red')
grid on
hold off, legend('Observation', 'Fitted Model', 'Prediction', 'location', 'NorthWest')
title(sprintf('US population predicted from %d to %d', ... 
                cdate(N), cdatePred(end)))
xlabel('Census Year'), ylabel('Population (millions)')
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex6-2-f.pdf")

pop(cdate == 1950 | cdate == 1970 | cdate == 1990)

Gpred*m
