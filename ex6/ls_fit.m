function [m, y] = ls_fit(mSysParam, L)
    % mSysParam ... input pair vector of (zi, di) [N x 2]
    % zi        ... input to the system
    % di        ... output of the system
    % L         ... order of the polynomial
    
    N = length(mSysParam(:,1));
    
    G = [ones(N, 1), mSysParam(:,1), zeros(N, L-1)]; % first order
    if L > 1
        for k = 3:L+1
            G(:,k) = mSysParam(:,1).^(k-1); % add further orders
        end
    end
    
    m = pinv(G)*mSysParam(:,2); % model parameter
    
    y = G*m; % output according to model 
    
end