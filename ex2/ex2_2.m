% creating an example with matlab fft
f = 2 % Hz
L = 2 % s
Fs = 10 % Hz
Ts = 1/Fs % s

t = 0:Ts:(L-Ts)

s1 = sin(2*pi*f*t);
n = length(s1);

X = dft(s1);
fshift = (-n/2:n/2-1)*(Fs/n); % zero-centered frequency range

stem(t, s1)
xlabel('Time [s]')
ylabel('Amplitude [1]')

saveas(gcf, 'BSP_ex2_2b_signal.png')

figure;
stem(fshift,abs(fftshift(X)), 'r')
xlabel('Frequency [Hz]')
grid on;
xticks(-10:1:10)
saveas(gcf, 'BSP_ex2_2b_spectrum.png')

figure;
% superposition of sine wave
f1 = 3;
f2 = 6;

s2 = sin(2*pi*f1*t) + sin(2*pi*f2*t);
n = length(s2);

X2 = dft(s2);
fshift = (-n/2:n/2-1)*(Fs/n); % zero-centered frequency range

stem(fshift,abs(fftshift(X2)), 'r')
grid on;
xlabel('Frequency [Hz]')
xticks(-10:1:10)
saveas(gcf, 'BSP_ex2_2c_spectrum.png')