% -------------------------------------------------------------------------
% Biosignal Processing - Excercise 07
% Group members:
% + Patrick Schuster - 51811485
% + Daniel Uhl - 11730101
% -------------------------------------------------------------------------

% clean workspace
clc; clear; close all;

%% set input parameters and plot input signal
fs = 100; % Hz
Tp = 5-1/fs;
t = (0:1/fs:Tp).';
f = [5 25 40].';
y = sum(sin(2*pi*f*t.')).';
N = length(y);

Tp_os = 0.4;
fos = 1000; % Hz
tos = (0:1/fos:Tp_os).';
y_ = sum(sin(2*pi*f*tos.'));
k = fos/fs;

figure;
subplot(2,1,1)
plot(t,y), grid on;
ylabel('Amplidute'), xlabel('Time in seconds');
title(sprintf('Sum of sine with f = [%d, %d, %d]', f(1), f(2), f(3)));
subplot(2,1,2)
plot(t, sin(2*pi*f(2)*t.')), grid on;


figure;
subplot(2,1,1)
plot(tos, y_), grid on, hold on
plot(tos(1:k:end), y_(1:k:end), '.r'), hold off
ylabel('Amplidute'), xlabel('Time in seconds');
title(sprintf('Sum of sine with f = [%d, %d, %d] Hz', f(1), f(2), f(3)));
subplot(2,1,2)
y1 = sin(2*pi*f(1)*tos.');
y2 = sin(2*pi*f(2)*tos.');
y3 = sin(2*pi*f(3)*tos.');
plot(tos, sin(2*pi*f*tos.')), grid on, hold on;
plot(tos(1:k:end), y1(1:k:end), '.r'), plot(tos(1:k:end), y2(1:k:end), '.r'),
plot(tos(1:k:end), y3(1:k:end), '.r'), hold off;
ylabel('Amplidute'), xlabel('Time in seconds');
title(sprintf('Indivudual sine waves'));
legend('f = 5 Hz', 'f = 25 Hz', 'f = 40 Hz');
clear y1 y2 y3

%% spectrum

f_norm = (-N/2:1:(N-1)/2).'/N;
f./fs

Y = fft(y).';
Y = Y./N;

figure;
stem(f_norm, fftshift(abs(Y))), grid on;
title('Amplitude Spectrum'); xlabel('normalized frequency');
ylabel('Amplitude');

figure;
plot(f_norm, 20*log10(fftshift(abs(Y)))), grid on;
title('Amplitude Spectrum'); xlabel('normalized frequency');
ylabel('Amplitude');

%% Filter design
%% lowpass filter

% lowpass filter: suppress f2 and f3
% fir1 takes as input: order, frequency constraints, filtertype and window
% the frequncy has to be normalized to the Nyquist frequency: fN = fs/2 !!!
% for the design the whole frequency range has been considered -> i.e. to
% obtain a cut-off freq. at 0.1 (10 Hz) a multiplication by 2 is needed
ord = 40;
b_lpf_rect = fir1(ord, 0.1*2, rectwin(ord+1), 'low').';
b_lpf_ham = fir1(ord, 0.1*2, hamming(ord+1), 'low').';

% frequency response fir design with rectangular window
[h_lpf_rect, w] = freqz(b_lpf_rect, 1, N);
[h_phs_lpf_rect, ~] = phasez(b_lpf_rect, 1, N);
% frequency response fir design with hamming window
[h_lpf_ham, ~] = freqz(b_lpf_ham, 1, N);
[h_phs_lpf_ham, ~] = phasez(b_lpf_ham, 1, N);
w = w./(2*pi);

figure;
subplot(2,1,1)
plot(w, 20*log10(abs(h_lpf_rect))), grid on, hold on;
plot(w, 20*log10(abs(h_lpf_ham))), hold off;
legend('rectwin', 'hamming')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF');
subplot(2,1,2)
plot(w, h_phs_lpf_rect./pi*180), grid on, hold on;
plot(w, h_phs_lpf_ham./pi*180), hold off;
legend('rectwin', 'hamming')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - LPF');


y_lpf = filter(b_lpf_ham, 1, y).';
Y_lpf = fft(y_lpf)./length(y_lpf);
Y_lpf2 = fft(y_lpf(101:end))./length(y_lpf(101:end));
N_short = length(y_lpf(101:end));
fn_short = (-N_short/2:1:(N_short-1)/2).'/N_short;

% bi-directional filtering
y_lpf_bi = filtfilt(b_lpf_ham, 1, y).';
Y_lpf_bi = fft(y_lpf_bi)./length(y_lpf_bi);


figure;
plot(t, y_lpf), grid on, hold on
plot(t, y_lpf_bi)
xlabel('Time in seconds'), ylabel('Amplitude');
title('Filtered signal'), legend('single', 'bi-dir');


figure;
subplot(2,1,1)
plot(f_norm, 20*log10(fftshift(abs(Y_lpf)))), grid on, hold on;
plot(f_norm, 20*log10(fftshift(abs(Y_lpf_bi))))
title('Magnitude Response - whole'); legend('single', 'bi-dir');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
subplot(2,1,2)
plot(fn_short, 20*log10(fftshift(abs(Y_lpf2)))), grid on
title('Magnitude Response - Skipped initial phase'); legend('single');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
axis([-inf inf -80 10])

%% bandpass filter

% bandpass filter: suppress f1 and f3
% fir1 takes as input: order, frequency constraints, filtertype and window
% the frequncy has to be normalized to the Nyquist frequency: 0 ... 1 (fs)
ord = 40;
b_bpf_rect = fir1(ord, [0.2 0.3].*2, rectwin(ord+1), 'bandpass').';
b_bpf_ham = fir1(ord, [0.2 0.3].*2, hamming(ord+1), 'bandpass').';

% frequency response fir design with rectangular window
[h_bpf_rect, w] = freqz(b_bpf_rect, 1, N);
[h_phs_bpf_rect, ~] = phasez(b_bpf_rect, 1, N);
% frequency response fir design with hamming window
[h_bpf_ham, ~] = freqz(b_bpf_ham, 1, N);
[h_phs_bpf_ham, ~] = phasez(b_bpf_ham, 1, N);
w = w./(2*pi);

figure;
subplot(2,1,1)
plot(w, 20*log10(abs(h_bpf_rect))), grid on, hold on;
plot(w, 20*log10(abs(h_bpf_ham))), hold off;
legend('rectwin', 'hamming')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - BPF');
subplot(2,1,2)
plot(w, h_phs_bpf_rect./pi*180), grid on, hold on;
plot(w, h_phs_bpf_ham./pi*180), hold off;
legend('rectwin', 'hamming')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - BPF');

y_bpf = filter(b_bpf_ham, 1, y).';
Y_bpf = fft(y_bpf)./(length(y_bpf));
Y_bpf2 = fft(y_bpf(101:end))./(length(y_bpf(101:end)));
N_short = length(y_bpf(101:end));
fn_short = (-N_short/2:1:(N_short-1)/2).'/N_short;

% bi-directional filtering
y_bpf_bi = filtfilt(b_bpf_ham, 1, y).';
Y_bpf_bi = fft(y_bpf_bi)./length(y_bpf_bi);

figure;
plot(t, y_bpf), grid on, hold on
plot(t, y_bpf_bi)
xlabel('Time in seconds'), ylabel('Amplitude');
title('Filtered signal'), legend('single', 'bi-dir');

figure;
subplot(2,1,1)
plot(f_norm, 20*log10(fftshift(abs(Y_bpf)))), grid on, hold on;
plot(f_norm, 20*log10(fftshift(abs(Y_bpf_bi))))
title('Magnitude Response - whole'); legend('single', 'bi-dir');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
subplot(2,1,2)
plot(fn_short, 20*log10(fftshift(abs(Y_bpf2)))), grid on
title('Magnitude Response - Skipped initial phase'); legend('single');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
axis([-inf inf -80 10])

%% bandstop filter

% bandstop filter: suppress f2
% fir1 takes as input: order, frequency constraints, filtertype and window
% the frequncy has to be normalized to the Nyquist frequency: 0 ... 1 (fs)
ord = 40;
b_bsf_rect = fir1(ord, [0.1 0.35].*2, rectwin(ord+1), 'stop').';
b_bsf_ham = fir1(ord, [0.1 0.35].*2, hamming(ord+1), 'stop').';

% frequency response fir design with rectangular window
[h_bsf_rect, w] = freqz(b_bsf_rect, 1, N);
[h_phs_bsf_rect, ~] = phasez(b_bsf_rect, 1, N);
% frequency response fir design with hamming window
[h_bsf_ham, ~] = freqz(b_bsf_ham, 1, N);
[h_phs_bsf_ham, ~] = phasez(b_bsf_ham, 1, N);
w = w./(2*pi);

figure;
subplot(2,1,1)
plot(w, 20*log10(abs(h_bsf_rect))), grid on, hold on;
plot(w, 20*log10(abs(h_bsf_ham))), hold off;
legend('rectwin', 'hamming')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - BSF');
subplot(2,1,2)
plot(w, h_phs_bsf_rect./pi*180), grid on, hold on;
plot(w, h_phs_bsf_ham./pi*180), hold off;
legend('rectwin', 'hamming')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - BSF');

y_bsf = filter(b_bsf_ham, 1, y).';
Y_bsf = fft(y_bsf)./(length(y_bsf));
Y_bsf2 = fft(y_bsf(101:end))./length(y_bsf(101:end));
N_short = length(y_bsf(101:end));
fn_short = (-N_short/2:1:(N_short-1)/2).'/N_short;

% bi-directional filtering
y_bsf_bi = filtfilt(b_bsf_ham, 1, y).';
Y_bsf_bi = fft(y_bsf_bi)./(length(y_bsf_bi));

figure;
plot(t, y_bsf), grid on, hold on
plot(t, y_bsf_bi)
xlabel('Time in seconds'), ylabel('Amplitude');
title('Filtered signal'), legend('single', 'bi-dir');

figure;
subplot(2,1,1)
plot(f_norm, 20*log10(fftshift(abs(Y_bsf)))), grid on, hold on;
plot(f_norm, 20*log10(fftshift(abs(Y_bsf_bi))))
title('Magnitude Response - whole'); legend('single', 'bi-dir');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
subplot(2,1,2)
plot(fn_short, 20*log10(fftshift(abs(Y_bsf2)))), grid on
title('Magnitude Response - Skipped initial phase'); legend('single');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
axis([-inf inf -80 10])

%% lowpass filter

% bandstop filter: suppress f3
% fir1 takes as input: order, frequency constraints, filtertype and window
% the frequncy has to be normalized to the Nyquist frequency: 0 ... 1 (fs)
ord = 30;
b_lpf2_rect = fir1(ord, 0.3*2, rectwin(ord+1), 'low').';
b_lpf2_ham = fir1(ord, 0.3*2, hamming(ord+1), 'low').';

% frequency response fir design with rectangular window
[h_lpf2_rect, w] = freqz(b_lpf2_rect, 1, N);
[h_phs_lpf2_rect, ~] = phasez(b_lpf2_rect, 1, N);
% frequency response fir design with hamming window
[h_lpf2_ham, ~] = freqz(b_lpf2_ham, 1, N);
[h_phs_lpf2_ham, ~] = phasez(b_lpf2_ham, 1, N);
w = w./(2*pi);

figure;
subplot(2,1,1)
plot(w, 20*log10(abs(h_lpf2_rect))), grid on, hold on;
plot(w, 20*log10(abs(h_lpf2_ham))), hold off;
legend('rectwin', 'hamming')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF');
subplot(2,1,2)
plot(w, h_phs_lpf2_rect./pi*180), grid on, hold on;
plot(w, h_phs_lpf2_ham./pi*180), hold off;
legend('rectwin', 'hamming')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - LPF');

y_lpf2 = filter(b_lpf2_ham, 1, y).';
Y_lpf2 = fft(y_lpf2)./(length(y_lpf2));
Y_lpf22 = fft(y_lpf2(101:end))./length(y_lpf2(101:end));
N_short = length(y_lpf2(101:end));
fn_short = (-N_short/2:1:(N_short-1)/2).'/N_short;

% bi-directional filtering
y_lpf2_bi = filtfilt(b_lpf2_ham, 1, y).';
Y_lpf2_bi = fft(y_lpf2_bi)./(length(y_lpf2_bi));

figure;
plot(t, y_lpf2), grid on, hold on
plot(t, y_lpf2_bi)
xlabel('Time in seconds'), ylabel('Amplitude');
title('Filtered signal'), legend('single', 'bi-dir');


figure;
subplot(2,1,1)
plot(f_norm, 20*log10(fftshift(abs(Y_lpf2)))), grid on, hold on;
plot(f_norm, 20*log10(fftshift(abs(Y_lpf2_bi))))
title('Magnitude Response - whole'); legend('single', 'bi-dir');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
subplot(2,1,2)
plot(fn_short, 20*log10(fftshift(abs(Y_lpf22)))), grid on
title('Magnitude Response - Skipped initial phase'); legend('single');
xlabel('normalized frequency f/f_s'), ylabel('Magnitude in dB');
axis([-inf inf -80 10])
