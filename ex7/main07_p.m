% -------------------------------------------------------------------------
% Biosignal Processing - Excercise 07
% Group members:
% + Patrick Schuster - 51811485
% + Daniel Uhl - 11730101
% -------------------------------------------------------------------------

% clean workspace
clear; 
clc;
close all;
%%

fs = 100;

%% Create a magnitude response of an ideal high-pass filter with 101 points
%   stopband [-25,25] Hz, 
%   make sure that the spectrum is fully symmetric around zero.
f_axis = -fs/2:fs/2;
mr_hp = [ones(1,25) zeros(1,51) ones(1,25)];

% check for symmetry
assert(all(mr_hp(end:-1:(ceil(101/2)+1)) == mr_hp(1:floor(101/2))));

% spectrum goes from [-fs/2 to fs/2], i.e. -50 Hz to 50 Hz

%% Add a linear phase with a group delay of tau = 0.01s
tau = 0.01;
mr_hp = exp(-1j*2*pi*tau.*f_axis) .* mr_hp;
mr_hp_2 = exp(-1j*tau.*f_axis) .* mr_hp;
% should i leave the *2pi in here?
% Multiply with exp(j w tau) https://en.wikipedia.org/wiki/Linear_phase

figure;
subplot(2,1,1)
plot(f_axis, abs(mr_hp)), grid on;
subplot(2,1,2)
plot(f_axis, unwrap(atan2(imag(mr_hp), real(mr_hp)))), grid on;
title("Ideal high pass filter in frequency domain");
xlabel("Frequency");
ylabel("Amplitude");
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex7-1-mag-resp-ideal-hp.pdf")

%% Fourier Transform ideal Frequancy response
frequency_response = mr_hp;
impulse_response = fftshift(ifft(fftshift(frequency_response(1:end-1))));
impulse_response_2 = fftshift(ifft(fftshift(mr_hp_2(1:end-1))));

figure;
hold on; grid on;
stem(real(impulse_response));
stem(imag(impulse_response));
legend(["real(impulse\_response)", "imag(impulse\_response)"]);
xlabel("samples"); ylabel("amplitude"); hold off;
title("Impulse response of the ideal high-pass filter");
ylim([-0.5 1])

%% Zero-pad the impulse response symmetrically with 150 zeros on each side
zeropadded_impulse_response = [zeros(1,150) impulse_response zeros(1,150)]; 

figure;
hold on; grid on;
stem(abs(fftshift(ifft(fftshift([ones(1,25) zeros(1,51) ones(1,24)])))));
stem(abs(impulse_response)); 
stem(abs(impulse_response_2)); 
% stem(imag(zeropadded_impulse_response));
legend(["none", "linear pase \omega", "linear pahse f"]);
xlabel("samples"); ylabel("amplitude"); hold off;
title("Impulse response of the zero-padded high-pass filter");
ylim([-0.5 1])

%% Apply a fft to the zero-padded impulse response 
%   to see the 'true' frequency response, use fftshift
true_freq_response = fftshift(fft(zeropadded_impulse_response));


%% Plot filter spectrum and phase into one figure

% abs(spectrum) and phase of spectrum
figure;
hold on
plot(abs(true_freq_response));
plot(angle(true_freq_response)); 
legend(["abs(true\_freq\_response)", "angle(true\_freq\_response)"]);
hold off
title(sprintf('True Frequency Response of filter'))
xlabel('Frequency'), ylabel('Magnitude')
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex7-1-true-freq-resp.pdf")

% Re and Im of impulse response
figure;
hold on
plot(real(true_freq_response));
plot(imag(true_freq_response)); 
legend(["real(true\_freq\_response)", "imag(true\_freq\_response)"]);
hold off
title(sprintf('True Frequency Response of filter, %d to %d',123, 456))
xlabel('Frequency'), ylabel('Magnitude')
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex7-1-true-freq-resp.pdf")



%% Test Signal
fs = 100; % Hz
L = 10; % s
frequencies = [15 35];
amplitudes = [1 1];

t = 0:1/fs:L;
test_signal = zeros(1, L*fs+1);

for i = 1:length(frequencies)
    test_signal = test_signal + amplitudes(i) .* sin(2*pi*frequencies(i)*t);
end
N_ts = length(test_signal);

% apply filter
% filtered_sig = conv(test_signal, real(impulse_response));
filtered_sig = filter(real(zeropadded_impulse_response), 1, test_signal);
f_ts = (-N_ts/2:1:(N_ts-1)/2)*fs/N_ts;
N_conv = length(filtered_sig);
f_conv = (-N_conv/2:1:(N_conv-1)/2)*fs/N_conv;
N_tfr = length(zeropadded_impulse_response);
f_tfr = (-N_tfr/2:1:(N_tfr-1)/2)*fs/N_tfr;

% plot spectrum of test signal
figure;
hold on; grid on;
title("Spectrum of test signal");
stem(f_ts, abs(fftshift(fft(test_signal))));
stem(f_conv, abs(fftshift(fft(filtered_sig))));

xlabel('Frequency'), ylabel('Magnitude')
hold off;
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex7-1-test-spectrum.pdf")

% magnitude spectrum of the filtered signal
figure;
hold on; grid on;
title("Spectrum of test signal");
plot(f_ts, 20*log10(abs(fftshift(fft(test_signal)))));
plot(f_conv, 20*log10(abs(fftshift(fft(filtered_sig)))));
plot(f_tfr, 20*log10(abs(true_freq_response)));

xlabel('Frequency'), ylabel('Magnitude in dB')
hold off; axis([-inf inf -50 60])
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex7-1-mag-spectrum.pdf")
%% filter test signal
% assert(0);

% plot spectrum of filtered test signal
figure;
hold on; grid on;
title("Filtered test signal");
plot(filtered_sig);
plot(test_signal);
xlabel('Frequency'), ylabel('Magnitude')
hold off;
set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex7-1-test-spectrum.pdf")

