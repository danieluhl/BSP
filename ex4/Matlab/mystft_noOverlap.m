function [XY] = mystft_noOverlap(X, fs, win)
    win_length = length(win)
    sig_length = length(X)
    
    % Nyquist frequency of the signal
    f_max = fs / 2
    
    XY = zeros(win_length, ceil(sig_length / win_length))
    whos
    % loop over full time range
    for i = 1:size(XY,2)-1
        % generate time slice (lenght 1 window)
        windowed_slice = X(win_length*(i-1)+1:win_length*i) .* win % (1, win_length);  
        
        % zero padding
        
       XY(:,i) = fftshift(fft(windowed_slice))
    end
end