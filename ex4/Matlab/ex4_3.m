X = load("ecg_data.mat");

fs = 360;

[res1, F1, T1] = mystft(X.d.S1(:,1),  fs, rectwin(  32),   32*0.75, 10);
[res2, F2, T2] = mystft(X.d.S1(:,1),  fs, rectwin(  64),   64*0.75, 10);
[res3, F3, T3] = mystft(X.d.S1(:,1),  fs, rectwin( 128),  128*0.75, 10);
[res4, F4, T4] = mystft(X.d.S1(:,1),  fs, rectwin( 256),  256*0.75, 10);
[res5, F5, T5] = mystft(X.d.S1(:,1),  fs, rectwin( 512),  512*0.75, 10);
[res6, F6, T6] = mystft(X.d.S1(:,1),  fs, rectwin(4096), 4096*0.75, 100);
[res7, F7, T7] = mystft(X.d.S1(:,1),  fs, rectwin(4096), 4096*0.75, 100);
[res8, F8, T8] = mystft(X.d.S1(:,1),  fs, rectwin(4096), 4096*0.75, 100);
[res9, F9, T9] = mystft(X.d.S1(:,1),  fs, rectwin(4096), 4096*0.75, 100);

x_roi = [0, 1800]
y_roi = [0, 40]


figure(1)
imagesc(T1, F1, 20*log10(abs(res1)));
colorbar;
caxis([-30, 30]);
xlim(x_roi);
ylim(y_roi);


figure(2)
imagesc(T2, F2, 20*log10(abs(res2)));
colorbar;
caxis([-30, 30]);
xlim(x_roi);
ylim(y_roi);


figure(3)
imagesc(T3, F3, 20*log10(abs(res3)));
colorbar;
caxis([-30, 30]);
xlim(x_roi);
ylim(y_roi);


figure(4)
imagesc(T4, F4, 20*log10(abs(res4)));
colorbar;
caxis([-30, 30]);
xlim(x_roi);
ylim(y_roi);

figure(5)
imagesc(T5, F5, 20*log10(abs(res5)));
colorbar;
xlim(x_roi);
ylim(y_roi);

figure(6)
imagesc(T6, F6, 20*log10(abs(res6)));
colorbar;
xlim(x_roi);
ylim(y_roi);

figure(7)
imagesc(T7, F7, 20*log10(abs(res7)));
colorbar;
xlim(x_roi);
ylim(y_roi);

figure(8)
imagesc(T8, F8, 20*log10(abs(res8)));
colorbar;
xlim(x_roi);
ylim(y_roi);

figure(9)
imagesc(T9, F9, 20*log10(abs(res9)));
colorbar;
xlim(x_roi);
ylim(y_roi);
