function [FT, F, T] = mystft(input_signal, fs, win, overlap_length, zero_pad)
    win_length = length(win);
    sig_length = length(input_signal);
    
    F_length = win_length + 2 * zero_pad;
    T_length = floor(sig_length / (win_length - overlap_length));
    FT = zeros(F_length, T_length);
    fprintf("y: %d, x: %d\n", F_length, T_length);
    
    for i = 1:T_length
        slice_start = max(1, (win_length - overlap_length) * (i-1)  - 1);        
        slice_stop = slice_start + win_length - 1;
        fprintf("%d\t Slice from %d to %d\n", i, slice_start, slice_stop);
        
        slice = input_signal(slice_start:min(slice_stop, sig_length));
        
        if (slice_stop > sig_length)
            % if there is misalignment between the window and the signal
            % (i.e. the signal length is not a multiple of the window
            % length)
            zero_padding_nonex = zeros(slice_stop - sig_length, 1);
            slice = [slice; zero_padding_nonex];
        end
        windowed_slice =  slice .* win;
        
        if (zero_pad)
            %   Signal should be padded on both sides symmetrically, number of
            %   zeroes is variable (parameter zero_pad), eg.
            %            _______
            %   ________/       \_______
            %   |       |       |       |
            %      pad     win     pad
            pad = zeros(zero_pad, 1); 
            windowed_slice = [pad; windowed_slice; pad];
        end
        FT(:,i) = fftshift(fft(windowed_slice));
    end
    
    % first element is win_length / 2 * 1/fs
    % then every element is spaced win_length * 1/fs apart
    T = (win_length/2:win_length-overlap_length:sig_length-(win_length/2))/fs;
    % linearly between +- nyquist frequency
    F = (-F_length/2-1:F_length/2)*(fs/F_length);
end