X = load("signal_stft_exercise.mat");
fs = 100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% window large
[res, F, T] = mystft(X.signal_vector, fs, hann(256), 192, 10);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-window_large.pdf")
% window small
[res, F, T] = mystft(X.signal_vector, fs, hann(16), 8, 10);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-window_small.pdf")
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rectangular
[res, F, T] = mystft(X.signal_vector, fs, rectwin(128), 96, 10);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-rect_win.pdf")
% Hamming
[res, F, T] = mystft(X.signal_vector, fs, hamming(128), 96, 10);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-hamming_win.pdf")
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% with zeropad
[res, F, T] = mystft(X.signal_vector, fs, hann(128), 96, 10);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-with_zeropad.pdf")
% without zeropad
[res, F, T] = mystft(X.signal_vector, fs, hann(128), 96, 0);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-without_zeropad.pdf")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% max overlap
[res, F, T] = mystft(X.signal_vector, fs, hann(128), 102, 10);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-max_overlap.pdf")
% no overlap
[res, F, T] = mystft(X.signal_vector, fs, hann(128), 1, 10);
figure;
imagesc(T, F, 20*log10(abs(res))); colorbar; caxis([-30, 30]);
set(gcf, 'PaperPosition', [0 0 5 5]);
set(gcf, 'PaperSize', [5 5]);
saveas(gcf, "../images/task_02/ex4-2-no_overlap.pdf")
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





