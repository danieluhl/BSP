% -------------------------------------------------------------------------
% Biosignal Processing - Excercise 07
% Group members:
% + Patrick Schuster - 51811485
% + Daniel Uhl - 11730101
% -------------------------------------------------------------------------

% clean workspace
clear; 
clc;
close all;
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Taks 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% filter order
M = [10 50 100].';
% normalized cut-off frequency (normalized to fs not fN!)
fc = 0.25; % half of the normalized Nyquist frequency
N = 1024;

% 'fir1' and 'butter' fcn provide frequencies up to Nyquist frequency 
% -> double normalized frequency to have the right scale again

max_ord = max(M)+1;
b_iir = zeros(max_ord, length(M));
a_iir = zeros(max_ord, length(M));
b_fir = zeros(max_ord, length(M));

h_iir = zeros(N, length(M));
phs_iir = zeros(N, length(M));
h_fir = zeros(N, length(M));
phs_fir = zeros(N, length(M));

for k_design = 1:length(M)
    % IIR filter design
    [bi_lpf, ai_lpf] = butter(M(k_design), fc*2, 'low');    
    b_iir(1:length(bi_lpf), k_design) = bi_lpf;
    a_iir(1:length(bi_lpf), k_design) = ai_lpf;
    
    % frequency response IIR design with butterworth characteristics
    [hi_lpf, ~] = freqz(bi_lpf, ai_lpf, N);
    [hi_phs_lpf, ~] = phasez(bi_lpf, ai_lpf, N);
    h_iir(:,k_design) = hi_lpf;
    phs_iir(:,k_design) = hi_phs_lpf;
    
    % FIR filter design
    bf_lpf = fir1(M(k_design), fc*2, rectwin(M(k_design)+1), 'low').';   
    b_fir(1:length(bf_lpf), k_design) = bf_lpf;
    
    % frequency response fir design with rectangular window
    [hf_lpf, w] = freqz(bf_lpf, 1, N);
    [hf_phs_lpf, ~] = phasez(bf_lpf, 1, N);
    h_fir(:,k_design) = hf_lpf;
    phs_fir(:,k_design) = hf_phs_lpf;
end

w = w./(2*pi);

%% visualization of the FIR filters

% whole spectra of the FIR filters
figure;
%  subplot(2,1,1)
plot(w, 20*log10(abs(h_fir(:,1)))), grid on, hold on;
plot(w, 20*log10(abs(h_fir(:,2))));
plot(w, 20*log10(abs(h_fir(:,3)))), hold off;
legend('order: 10', 'order: 50', 'order: 100', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF - FIR filters');
axis([-inf inf -70 5]);
%{
subplot(2,1,2)
plot(w, phs_fir(:,1)./pi*180), grid on, hold on;
plot(w, phs_fir(:,2)./pi*180)
plot(w, phs_fir(:,3)./pi*180), hold off;
legend('order: 10', 'order: 50', 'order: 100', 'location', 'SouthWest')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - LPF');
%}

% zoomed in version of the FIR filters spctra
figure;
plot(w, 20*log10(abs(h_fir(:,1)))), grid on, hold on;
plot(w, 20*log10(abs(h_fir(:,2))));
plot(w, 20*log10(abs(h_fir(:,3)))), hold off;
legend('order: 10', 'order: 50', 'order: 100', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF - FIR filters');
axis([-inf 0.3 -25 5]);

%% visualization of the IIR filters

% whole spectra of the IIR filters
figure;
%  subplot(2,1,1)
plot(w, 20*log10(abs(h_iir(:,1)))), grid on, hold on;
plot(w, 20*log10(abs(h_iir(:,2))));
plot(w, 20*log10(abs(h_iir(:,3)))), hold off;
legend('order: 10', 'order: 50', 'order: 100', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF - IIR filters');
axis([-inf inf -70 5]);
%{
subplot(2,1,2)
plot(w, phs_fir(:,1)./pi*180), grid on, hold on;
plot(w, phs_fir(:,2)./pi*180)
plot(w, phs_fir(:,3)./pi*180), hold off;
legend('order: 10', 'order: 50', 'order: 100', 'location', 'SouthWest')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - LPF');
%}

% zoomed in version of the IIR filters spctra
figure;
plot(w, 20*log10(abs(h_iir(:,1)))), grid on, hold on;
plot(w, 20*log10(abs(h_iir(:,2))));
plot(w, 20*log10(abs(h_iir(:,3)))), hold off;
legend('order: 10', 'order: 50', 'order: 100', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF - IIR filters');
axis([-inf 0.3 -25 5]);

%% visualization IIR vs FIR

figure;
subplot(3,1,1)
plot(w, 20*log10(abs(h_fir(:,1)))), grid on, hold on;
plot(w, 20*log10(abs(h_iir(:,1))));
legend('FIR', 'IIR', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF of order 10');
axis([-inf inf -70 5]);

subplot(3,1,2)
plot(w, 20*log10(abs(h_fir(:,2)))), grid on, hold on;
plot(w, 20*log10(abs(h_iir(:,2))));
legend('FIR', 'IIR', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF of order 50');
axis([0.15 0.3 -40 5]);

subplot(3,1,3)
plot(w, 20*log10(abs(h_fir(:,3)))), grid on, hold on;
plot(w, 20*log10(abs(h_iir(:,3))));
legend('FIR', 'IIR', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF of order 100');
axis([0.15 0.3 -40 5]);
%% visualization of the phase responses

figure;
plot(w, phs_fir(:,2)./pi*180), grid on, hold on;
plot(w, phs_iir(:,2)./pi*180), hold off;
legend('FIR', 'IIR', 'location', 'SouthWest')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - LPF of order 50');

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Taks 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fs = 100;   % Hz
L = 2.5;    % seconds
fi = [22 28].'; 

t = (0:1/fs:L-1/fs).';
y = sum(sin(2*pi*fi*t.')).';
y_20hz = sin(2*pi*fi(1)*t.').';
Y = fftshift(fft(y,N));

% IIR filtered signals
y_filt = zeros(length(y), length(M));
Y_filt = zeros(N, length(M));
% FIR filtered signals
yf_filt = zeros(length(y), length(M));
Yf_filt = zeros(N, length(M));

for l_filt = 1:length(M)
    % apply IIR filter
    y_filt(:,l_filt) = filter(b_iir(1:M(l_filt)+1,l_filt), ...
                                    a_iir(1:M(l_filt)+1,l_filt), y);
    Y_filt(:,l_filt) = fftshift(fft(y_filt(:,l_filt), N));
    
    % apply FIR filter
    yf_filt(:,l_filt) = filter(b_fir(:,l_filt), 1, y);
    Yf_filt(:,l_filt) = fftshift(fft(yf_filt(:,l_filt), N));
end

fn = (-N/2:1:(N-1)/2).'/N;

% bi-directionally filtering
y_bifilt = filtfilt(b_iir(1:M(2)+1,2), a_iir(1:M(2)+1,2), y).';
Y_bifilt = fftshift(fft(y_bifilt, N));

%% visualization

% transient response FIR filter
figure;
subplot(4,1,1)
plot(t(1:end/2), y_20hz(1:end/2)), grid on
axis([-inf inf -1 1])
ylabel('Amplitude'), title(sprintf('Sine wave with %d Hz', fi(1)))
subplot(4,1,2)
plot(t(1:end/2), yf_filt((1:end/2),1)), grid on
axis tight, ylabel('Amplitude'), title('IIR filter with order 10')
subplot(4,1,3)
plot(t(1:end/2), yf_filt((1:end/2),2)), grid on
axis tight, ylabel('Amplitude'), title('IIR filter with order 50')
subplot(4,1,4)
plot(t(1:end/2), yf_filt((1:end/2),3)), grid on
axis tight, xlabel('Time in seconds'), ylabel('Amplitude')
title('IIR filter with order 100')


% transient response IIR filter
figure;
subplot(4,1,1)
plot(t(1:end/2), y_20hz(1:end/2)), grid on
axis([-inf inf -1 1])
ylabel('Amplitude'), title(sprintf('Sine wave with %d Hz', fi(1)))
subplot(4,1,2)
plot(t(1:end/2), y_filt((1:end/2),1)), grid on
axis tight, ylabel('Amplitude'), title('IIR filter with order 10')
subplot(4,1,3)
plot(t(1:end/2), y_filt((1:end/2),2)), grid on
axis tight, ylabel('Amplitude'), title('IIR filter with order 50')
subplot(4,1,4)
plot(t(1:end/2), y_filt((1:end/2),3)), grid on
axis tight, xlabel('Time in seconds'), ylabel('Amplitude')
title('IIR filter with order 100')

% Magnitude spectrum in dB (FFT not normalized) - IIR filter
figure;
plot(fn, 20*log10(abs(Y_filt))), hold on, grid on
legend('order: 10', 'order: 50', 'order: 100', 'location', 'North')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Spectrum - LPF - IIR filter');
axis([-0.5 0.5 10 45]);

% Amplitude spectrum (FFT not normalized) - IIR flter
figure;
plot(fn, abs(Y_filt)), hold on, grid on
legend('order: 10', 'order: 50', 'order: 100', 'location', 'North')
ylabel('Amplitude (not normalized)'), xlabel('normalized frequncy f/f_s');
title('Amplidute Spectrum - LPF - IIR filter');
axis([-0.5 0.5 0 125]);

% Amplitude spectrum (FFT not normalized) - FIR flter
figure;
plot(fn, abs(Yf_filt)), hold on, grid on
legend('order: 10', 'order: 50', 'order: 100', 'location', 'North')
ylabel('Amplitude (not normalized)'), xlabel('normalized frequncy f/f_s');
title('Magnitude Spectrum - LPF - FIR filter');
axis([-0.5 0.5 0 115]);


%{
figure;
line(fn, 20*log10(abs(Y_bifilt)), 'Color','r')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude & Phase Response - Bidirectional filter');
ax1 = gca; % current axes
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos, 'YAxisLocation','right', 'Color','none');
line(fn, angle(Y_bifilt).*180/pi, 'Parent',ax2, 'Color','k')
axis([-0.5 0.5 -5 45]), grid on;
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
%}

% -------------------------------------------------------------------------
% bi-directionaly filtered
% -------------------------------------------------------------------------
% time response
figure;
subplot(3,1,1)
plot(t(1:end/2), y_20hz(1:end/2)), grid on
axis([-inf inf -1 1])
ylabel('Amplitude'), title(sprintf('Sine wave with %d Hz', fi(1)))
subplot(3,1,2)
plot(t(1:end/2), y_bifilt((1:end/2))), grid on
axis([-inf inf -1 1]), ylabel('Amplitude');
title('IIR filter with order 50 - bidirecionally applied')
subplot(3,1,3)
plot(t(1:end/2), y_filt((1:end/2),2)), grid on
axis([-inf inf -1 1]), ylabel('Amplitude');
title('IIR filter with order 50 - single'), xlabel('Time in seconds');


% angle
figure;
subplot(3,1,1)
plot(fn, angle(Y).*180/pi), grid on, hold on;
title('Phase Response - input signal'), ylabel('Phase in deg');
subplot(3,1,2)
plot(fn, angle(Y_bifilt).*180/pi)
title('Phase Response - IIR bi-directionally filtered');
ylabel('Phase in deg');
subplot(3,1,3)
plot(fn, angle(Y_filt(:,2)).*180/pi)
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - IIR single filtered');

% frequency domain
figure;
plot(fn, abs(Y_filt(:,2))), hold on, grid on
plot(fn, abs(Y_bifilt))
legend('IIR single', 'IIR bi-dir', 'location', 'North')
ylabel('Amplitude (not normalized)'), xlabel('normalized frequncy f/f_s');
title('Amplidute Spectrum - LPF - IIR filter');
axis([-0.5 0.5 0 130]);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Taks 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ord = 250;
fc = 0.25;  % normalized to sampling frequency

[bi_lpf, ai_lpf] = butter(ord, fc*2, 'low');   
[hi_lpf, w] = freqz(bi_lpf, ai_lpf, N);
[hi_phs_lpf, ~] = phasez(bi_lpf, ai_lpf, N);
    
w = w./(2*pi);

% whole spectra of the IIR filters
figure;
subplot(2,1,1)
plot(w, 20*log10(abs(hi_lpf))), grid on, hold on;
legend('order: 250', 'location', 'SouthWest')
ylabel('Magnitude in dB'), xlabel('normalized frequncy f/f_s');
title('Magnitude Response - LPF - IIR filter');
% axis([-inf inf -70 5]);
subplot(2,1,2)
plot(w, hi_phs_lpf./pi*180), grid on, hold on;
legend('order: 250', 'location', 'SouthWest')
ylabel('Phase in deg'), xlabel('normalized frequncy f/f_s');
title('Phase Response - LPF - IIR filter');


