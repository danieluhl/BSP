function Ret = phi(vx)

% rectangular function
% 1     for 0 <= x <= pi
% -1    for pi < x < 2*pi

vx = vx(:);
% vx = mod(vx, 1);
Ret = zeros(numel(vx), 1);

% Note: && -> operator for scalar
%       & -> operator for vectors
Ret(vx >= 0 & vx < 1) = 1;
Ret(vx > 1) = 0;
