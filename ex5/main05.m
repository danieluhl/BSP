% -------------------------------------------------------------------------
% Biosignal Processing - Excercise 05
% Group members:
% + Patrick Schuster - 51811485
% + Daniel Uhl - 11730101
% -------------------------------------------------------------------------

% clean workspace
clear; 
clc;
close all;
%%

v0 = [1 1 -1 -1 -1 -1 -2 2].';

% get subvectors
Nv = length(v0);
m = log2(Nv);
V(:,1) = v0;
for i = 1:2:2*m
    [vm_temp, wm_temp] = subvectors(V(:,i));
    V(1:length(wm_temp), i+1) = wm_temp;
    V(length(wm_temp)+1:Nv, i+1) = zeros(Nv-length(wm_temp), 1);
    V(1:length(vm_temp), i+2) = vm_temp;
    V(length(vm_temp)+1:Nv, i+2) = zeros(Nv-length(vm_temp), 1);
end

% clear dummy variables
clear vm_temp wm_temp

figure;
subplot(4,2,[1 2])
stem(0:Nv-1, V(:,1)), grid on, title('v^0');
subplot(4,2,3)
stem(0:Nv/2-1, V(1:Nv/2,3)), grid on, title('v^1');
subplot(4,2,4)
stem(0:Nv/2-1, V(1:Nv/2,2)), grid on, title('w^1');
subplot(4,2,5)
stem(0:Nv/4-1, V(1:Nv/4,5)), grid on, title('v^2');
subplot(4,2,6)
stem(0:Nv/4-1, V(1:Nv/4,4)), grid on, title('w^2');
subplot(4,2,7)
stem(0:Nv/8-1, V(1:Nv/8,7)), grid on, title('v^3');
subplot(4,2,8)
stem(0:Nv/8-1, V(1:Nv/8,6)), grid on, title('w^3');
sgtitle('scale coefficients')
%%
t = (0:0.1:Nv).';
y = zeros(length(t), 1);
for k = 0:Nv-1
    y = y + v0(k+1).*phi(t-k);
end

figure;
plot(t,y, 'LineWidth', 1.2), grid on;
xlabel('Time in seconds'), ylabel('Amplitude in unit')
legend('f(t)', 'location', 'SouthWest'), 
title(strcat('Function f with amplitudes (', ...
                sprintf('%d,', v0(1:end-1)), sprintf('%d)',v0(end))));
axis([-inf inf min(y)*1.1 max(y)*1.1]);
            
P = zeros(length(t), m);
Q = zeros(length(t), m);
for i = 1:m
    for k = 0:Nv-1
        P(:,i) = P(:,i) + V(k+1,2*i+1).*phi(2^-i.*t-k).*2^(-i/2);
        Q(:,i) = Q(:,i) + V(k+1,2*i).*psi_x(2^-i.*t-k).*2^(-i/2); 
    end
end

figure;
subplot(2,1,1)
plot(t, P, 'LineWidth', 1.2), grid on
legend('P_1', 'P_2', 'P_3');
xlabel('Time in seconds'), ylabel('Amplitude in unit')
axis([-inf inf min(min(P))*1.1 max(max(P))*1.1]);
subplot(2,1,2)
plot(t, Q, 'LineWidth', 1.2), grid on
legend('Q_1', 'Q_2', 'Q_3', 'location', 'SouthWest');
xlabel('Time in seconds'), ylabel('Amplitude in unit')
axis([-inf inf min(min(Q))*1.1 max(max(Q))*1.1]);


figure;
subplot(3,1,1)
plot(t, P(:,1)+Q(:,1), 'LineWidth', 1.2), grid on
legend('f = P_1+Q_1', 'location', 'SouthWest');
xlabel('Time in seconds'), ylabel('Amplitude in unit')
axis([-inf inf min(P(:,1)+Q(:,1))*1.1 max(P(:,1)+Q(:,1))*1.1]);
subplot(3,1,2)
plot(t, P(:,2)+Q(:,2), 'LineWidth', 1.2), grid on
legend('P_1 = P_2+Q_2');
xlabel('Time in seconds'), ylabel('Amplitude in unit')
axis([-inf inf min(P(:,2)+Q(:,2))*1.1 max(P(:,2)+Q(:,2))*1.1]);
subplot(3,1,3)
plot(t, P(:,3)+Q(:,3), 'LineWidth', 1.2), grid on
legend('P_2 = P_3+Q_3', 'location', 'SouthWest');
xlabel('Time in seconds'), ylabel('Amplitude in unit')
axis([-inf inf min(y)*1.1 max(y)*1.1]);
axis([-inf inf min(P(:,3)+Q(:,3))*1.1 max(P(:,3)+Q(:,3))+0.05]);
