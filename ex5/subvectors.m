function [vm, wm] = subvectors(v)

    v = v(:);
        
    vm = v(1:2:end) + v(2:2:end);
    vm = vm./sqrt(2);
    
    wm = v(1:2:end) - v(2:2:end);
    wm = wm./sqrt(2);
    
   