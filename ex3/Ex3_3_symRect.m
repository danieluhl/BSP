for N2 = [20, 200, 2000]
    N1 = N2/2

    rect_sig = [ones(1, N1/2), zeros(1, N2), ones(1, N1/2-1)]

    rect_spec       = fftshift(dft(rect_sig))
    rect_spec_zp    = [zeros(1, N1), rect_spec, zeros(1, N1)]
    sinc_idft       = idft(rect_spec_zp);

    fshift1 = (-(N2+N1)/2:(N2+N1)/2-2);
    fshift2 = (-(N2+N1)/2-N1:(N2+N1)/2+N1-2);

    tiledlayout(2,2)
    ax1 = nexttile;
    plot(rect_sig)
    title("Time Domain A");
    grid on;

    ax2 = nexttile;
    plot(fshift1, real(rect_spec));
    title("Frequency Domain A");
    grid on;

    ax3 = nexttile;
    plot(fshift2, real(rect_spec_zp));
    title("Frequency Domain B");
    grid on;

    ax4 = nexttile;
    plot(abs(sinc_idft));
    title("Time Domain B");
    text(N2, 0.9, "Max: " + num2str(max(abs(sinc_idft))));
    grid on;

    axis([ax4],[0 2*N2+N1 -0.01 1.1])
    set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
    set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
    saveas(gcf, "ex3-3-1N" + num2str(N2) + ".pdf")
    clear;
end
