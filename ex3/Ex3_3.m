clear;

N2 = 20
N1 = 5

figure(1);
% Generate a rect function
rect_sig = rectwin(N2-2*N1-1)
rect_sig = [zeros(N1,1); rect_sig; zeros(N1,1)]

% genearate a sinc function
my_sinc = sinc([-N1/5:0.1:N1/5-1/5]*pi*3/2)

% TODO: how can I plot this darn thing so that there is a higher
% resulution! I think there is a "sampling" issue so I always arrive at
% thie intersections with zero or sth. anyway I get a rectified signal :-/

% calculate the spectra
sinc_idft       = fftshift(abs(idft(my_sinc)))
rect_spec       = fftshift(real(dft(rect_sig)))
rect_spec_zp = [zeros(1, N1), rect_spec, zeros(1, N1)]

% plot rect function
subplot(2,2,1);
plot(rect_sig)
title("Time Domain A");
grid on;

subplot(2,2,2);
plot(rect_spec);
title("Frequency Domain A");
grid on;

subplot(2,2,3);
plot(sinc_idft);
title("Time Domain B");
text(N2/2 + N1, max(sinc_idft), num2str(max(sinc_idft)));
grid on;

subplot(2,2,4);
plot(my_sinc);
title("Frequency Domain B")
grid on;

set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex3-3-1N20.pdf")
% 
% 
% 
% clear;
% 
% N2 = 200
% N1 = 50
% 
% figure(2);
% % Generate a rect function
% rect_sig = rectwin(N2-2*N1)
% rect_sig = [zeros(N1,1); rect_sig; zeros(N1,1)]
% 
% % genearate a sinc function
% my_sinc = sinc([-N1+0.5:0.5:N1])*0.5
% 
% % TODO: how can I plot this darn thing so that there is a higher
% % resulution! I think there is a "sampling" issue so I always arrive at
% % thie intersections with zero or sth. anyway I get a rectified signal :-/
% 
% % calculate the spectra
% sinc_idft       = fftshift(abs(idft(my_sinc)))
% rect_spec       = fftshift(real(dft(rect_sig)))
% 
% % plot rect function
% subplot(2,2,1);
% plot(rect_sig)
% title("Time Domain A");
% grid on;
% 
% subplot(2,2,2);
% plot(rect_spec);
% title("Frequency Domain A");
% grid on;
% 
% subplot(2,2,3);
% plot(sinc_idft);
% title("Time Domain B");
% text(N2/2 + N1, max(sinc_idft), num2str(max(sinc_idft)));
% grid on;
% 
% subplot(2,2,4);
% plot(my_sinc);
% title("Frequency Domain B")
% grid on;
% 
% set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
% set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
% saveas(gcf, "ex3-3-1N200.pdf")
% 
% 
% 
% 
% clear;
% 
% N2 = 2000
% N1 = 500
% 
% figure(3);
% % Generate a rect function
% rect_sig = rectwin(N2-2*N1)
% rect_sig = [zeros(N1,1); rect_sig; zeros(N1,1)]
% 
% % genearate a sinc function
% my_sinc = sinc([-N1+0.5:0.5:N1])*0.5
% 
% % TODO: how can I plot this darn thing so that there is a higher
% % resulution! I think there is a "sampling" issue so I always arrive at
% % thie intersections with zero or sth. anyway I get a rectified signal :-/
% 
% % calculate the spectra
% sinc_idft       = fftshift(abs(idft(my_sinc)))
% rect_spec       = fftshift(real(dft(rect_sig)))
% 
% % plot rect function
% subplot(2,2,1);
% plot(rect_sig)
% title("Time Domain A");
% grid on;
% 
% subplot(2,2,2);
% plot(rect_spec);
% title("Frequency Domain A");
% grid on;
% 
% subplot(2,2,3);
% plot(sinc_idft);
% title("Time Domain B");
% text(N2/2 + N1, max(sinc_idft), num2str(max(sinc_idft)));
% grid on;
% 
% subplot(2,2,4);
% plot(my_sinc);
% title("Frequency Domain B")
% grid on;
% 
% set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
% set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
% saveas(gcf, "ex3-3-1N2000.pdf")









% % zero padding in the frequency domain

% rect_spec = [zeros(1,N2/10), rect_spec, zeros(1,N2/10)]
% stem(rect_spec)
% %plot(sinc(1:0.001:N2)
% inv_rect = idft(ifftshift(rect_spec))
% 
% figure(3);
% % show retransformation
% stem(inv_rect)
