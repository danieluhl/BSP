clear;
close all;
%%

N = 200;
rect = [ones(N/2,1); zeros(N/2,1)];

fRect = dft(rect);

figure;
subplot(2,1,1)
plot(rect), grid on, axis tight
title("Time Domain");
subplot(2,1,2)
plot(abs(fftshift(fRect))), grid on, axis tight;
title("Frequncy Domain");
sgtitle("Rectangle Transformation");

fRectPadded = ifftshift([zeros(N,1); transp(fftshift(fRect)); zeros(N,1)]);
rectPadded = idft(fRectPadded);


figure;
subplot(2,1,1)
plot(abs(fftshift(fRectPadded))), grid on, axis tight
title("Frequncy Domain");
subplot(2,1,2)
plot(real(rectPadded)), grid on, axis tight;
title("Time Domain");
sgtitle("Sinc Transformation");