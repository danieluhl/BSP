function x_vector = triangle(N)
    T_W = N
    t = -(T_W-1)/2 : 1 : (T_W-1)/2
    x_vector = transp(1 - (2*abs(t) / (N)))
     %                 1 -    2* 49 / 101
end
