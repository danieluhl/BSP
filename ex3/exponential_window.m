function x_vector = exponential_window(N)
    t = -(N-1)/2 : 1 : (N-1)/2;
    D = 8.69;
    tau = (N+1) * (8.69 / D)
    x_vector = transp(exp(-2*abs(t) / tau))
end
