function x_vector = triangle(N)
    t = -(N-1)/2 : 1 : (N-1)/2
    sigma = N / 5
    factor = 1/(sigma*sqrt(2*pi)) 
    x_vector = transp( factor * exp(-0.5*t.^2 / sigma^2))
end
