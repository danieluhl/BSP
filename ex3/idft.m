function X = idft(x)
  N = length(x)  
  X = zeros(1, N);
  for k = 1:N
    sum = 0;
    for n = 1:N
      sum = sum + x(n) * exp(1i * 2 * pi / N  * (k-1) * (n-1));
    end
  X(k) = sum;
  end
end