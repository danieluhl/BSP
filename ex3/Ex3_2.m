
% clear;
% figure();
% subplot(1,2,1)
% hold on;
% stem(triang(100), 'r');
% stem(triangle(100), 'k');
% stem(triangle(100) - triang(100), 'b');
% hold off;
% subplot(1,2,2)
% hold on;
% stem(dft(triangle(100)));
% stem(fft(triang(100)));
% hold off;


clear;
figure();
subplot(1,2,1)
%hold on;
stem(exponential_window(100), 'r');
%stem(triangle(100) - triang(100), 'b');
%hold off;
subplot(1,2,2)
%hold on;
stem(dft(exponential_window(100)));
%stem(fft(exponential_window(100)));
%hold off;


clear;
figure();
subplot(1,2,1)
hold on;
alpha = 2.475
stem(gaussian_window(100), 'r');
stem(gausswin(100, alpha), 'k');
stem(gaussian_window(100) - gausswin(100, alpha), 'b');
hold off;
subplot(1,2,2)
hold on;
stem(dft(gaussian_window(100)));
stem(fft(gausswin(100)));
hold off;


% % Triangle Window
% %stem(triangle(10), "k")
% %hold on;
% %stem(exponential_window(10),"k")
% figure(2)
% stem(gaussian_window(10),"k")
% hold on;
% stem(gausswin(10), "r")
% hold off;