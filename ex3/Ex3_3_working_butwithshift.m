clear;

N2 = 200
N1 = 100

% Generate a rectangular function
rect_sig = [ones(N2-N1, 1); zeros(N1,1)]-1
%rect_sig = [zeros(N2-N1, 1); ones(N1,1); zeros(N2-N1, 1)]

% calculate the spectra
rect_spec       = fftshift(dft(rect_sig))
rect_spec_zp    = [zeros(1, N1), rect_spec, zeros(1, N1)]
%rect_spec_zp    = [rect_spec, zeros(1, N1), zeros(1, N1)]
sinc_idft       = idft(rect_spec_zp);

figure(1)
hold on;
plot(real(rect_spec))
plot([-N2:0.01:N2]+N1, sinc([-N2:0.01:N2]/1.5)*0.5);
hold off;
saveas(figure(1), "ex3-3-rect_dft.pdf")

figure(2);
fshift1 = (-(N2+N1)/2:(N2+N1)/2-1);
fshift1 = (-N2/2:N2/2-1 );
fshift2 = (-(N2+N1)/2-N1:(N2+N1)/2+N1-1);
fshift2 = (-(N2)/2-N1:(N2)/2+N1-1);

tiledlayout(2,2)
% plot rect function
ax1 = nexttile;
plot(rect_sig)
title("Time Domain A");
grid on;

ax2 = nexttile;
hold on;
plot(fshift1, abs(rect_spec));
%plot(fshift1, imag(rect_spec), "r");
hold off;
title("Frequency Domain A");
grid on;


ax3 = nexttile;
hold on;
plot(fshift2, abs(rect_spec_zp));
%plot(fshift2, imag(rect_spec_zp), "r");
hold off;
title("Frequency Domain B");
grid on;


ax4 = nexttile;
plot(abs(sinc_idft));
%plot(real(sinc_idft));
%plot(imag(sinc_idft),"r");
title("Time Domain B");
text(N2 + N1, 0.9, "Max: " + num2str(max(abs(sinc_idft))));
grid on;


axis([ax2 ax3],[-(N1+N2) N1+N2 0 0.1])
%axis([ax2 ax3],[0 N1+N2 -0.25 0.25])

% 
% subplot(2,2,4);
% plot(my_sinc);
% title("Frequency Domain B")
% grid on;

set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
saveas(gcf, "ex3-3-1N20.pdf")
% 
% 
% 
% clear;
% 
% N2 = 200
% N1 = 50
% 
% figure(2);
% % Generate a rect function
% rect_sig = rectwin(N2-2*N1)
% rect_sig = [zeros(N1,1); rect_sig; zeros(N1,1)]
% 
% % genearate a sinc function
% my_sinc = sinc([-N1+0.5:0.5:N1])*0.5
% 
% % TODO: how can I plot this darn thing so that there is a higher
% % resulution! I think there is a "sampling" issue so I always arrive at
% % thie intersections with zero or sth. anyway I get a rectified signal :-/
% 
% % calculate the spectra
% sinc_idft       = fftshift(abs(idft(my_sinc)))
% rect_spec       = fftshift(real(dft(rect_sig)))
% 
% % plot rect function
% subplot(2,2,1);
% plot(rect_sig)
% title("Time Domain A");
% grid on;
% 
% subplot(2,2,2);
% plot(rect_spec);
% title("Frequency Domain A");
% grid on;
% 
% subplot(2,2,3);
% plot(sinc_idft);
% title("Time Domain B");
% text(N2/2 + N1, max(sinc_idft), num2str(max(sinc_idft)));
% grid on;
% 
% subplot(2,2,4);
% plot(my_sinc);
% title("Frequency Domain B")
% grid on;
% 
% set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
% set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
% saveas(gcf, "ex3-3-1N200.pdf")
% 
% 
% 
% 
% clear;
% 
% N2 = 2000
% N1 = 500
% 
% figure(3);
% % Generate a rect function
% rect_sig = rectwin(N2-2*N1)
% rect_sig = [zeros(N1,1); rect_sig; zeros(N1,1)]
% 
% % genearate a sinc function
% my_sinc = sinc([-N1+0.5:0.5:N1])*0.5
% 
% % TODO: how can I plot this darn thing so that there is a higher
% % resulution! I think there is a "sampling" issue so I always arrive at
% % thie intersections with zero or sth. anyway I get a rectified signal :-/
% 
% % calculate the spectra
% sinc_idft       = fftshift(abs(idft(my_sinc)))
% rect_spec       = fftshift(real(dft(rect_sig)))
% 
% % plot rect function
% subplot(2,2,1);
% plot(rect_sig)
% title("Time Domain A");
% grid on;
% 
% subplot(2,2,2);
% plot(rect_spec);
% title("Frequency Domain A");
% grid on;
% 
% subplot(2,2,3);
% plot(sinc_idft);
% title("Time Domain B");
% text(N2/2 + N1, max(sinc_idft), num2str(max(sinc_idft)));
% grid on;
% 
% subplot(2,2,4);
% plot(my_sinc);
% title("Frequency Domain B")
% grid on;
% 
% set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
% set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.
% saveas(gcf, "ex3-3-1N2000.pdf")









% % zero padding in the frequency domain

% rect_spec = [zeros(1,N2/10), rect_spec, zeros(1,N2/10)]
% stem(rect_spec)
% %plot(sinc(1:0.001:N2)
% inv_rect = idft(ifftshift(rect_spec))
% 
% figure(3);
% % show retransformation
% stem(inv_rect)
